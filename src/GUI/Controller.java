package GUI;

import Functionality.*;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

public class Controller {

    @FXML
    ImageView workoutStatusIconBd, workoutStatusIcon;
    @FXML
    Label workoutStatusLabel, weekAverageID;
    @FXML
    BarChart workoutStatistics;
    @FXML
    TableView<Statistics> tableView;
    @FXML
    TableColumn<Statistics, Integer> colWeekNumber;
    @FXML
    TableColumn<Statistics, Double> colExerciseHours;
    @FXML
    ImageView imgDefault, imgSlim, imgAverage, imgFat;

    public TextField inputHeight;
    public TextField inputWeight;
    public Label showBMI;
    public Label showBMICategory;

    public void handleBMI() {
        BMI bmi = new BMI();
        bmi.calculateBMI(inputWeight, inputHeight, showBMI, showBMICategory, imgDefault, imgSlim, imgAverage, imgFat);
    }

    public TextField weekNumber;
    public TextField totalHours;

    public void handleWorkoutTracker() {
        WorkoutTracker workout = new WorkoutTracker();
        workout.workoutTracking(weekNumber, totalHours);
        ChartHandler chart = new ChartHandler();
        chart.setChart(workoutStatusLabel, weekAverageID, workoutStatusIconBd, workoutStatusIcon, workoutStatistics);
        weekNumber.clear();
        totalHours.clear();
    }


    public void initialize() {

        HandleStatistics statistics = new HandleStatistics();
        statistics.addStatistics(tableView, colWeekNumber, colExerciseHours);

        ChartHandler chart = new ChartHandler();
        chart.setChart(workoutStatusLabel, weekAverageID, workoutStatusIconBd, workoutStatusIcon, workoutStatistics);

    }
}