package Database;

/**
 *
 * @author tha
 */
public class DBTester {
    
    public static void main (String [] args){

        DB.selectSQL("SELECT * FROM tblExercise;");
        System.out.println(DB.getDisplayData());

        do{
           String data = DB.getDisplayData();
           if (data.equals(DB.NOMOREDATA)){
               break;
           }else{
               System.out.print(data);
           }   
        } while(true);
        
        
        DB.deleteSQL("Delete from project where project_no like 'g%';");
        
        DB.selectSQL("Select project_name from project");
        
        System.out.println("numberOfColumns="+DB.getNumberOfColumns());
        do{
           String data = DB.getData();
           if (data.equals(DB.NOMOREDATA)){
               break;
           }else{
               System.out.print(data);
           }   
        } while(true);
       
    }
    
}
