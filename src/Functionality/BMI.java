package Functionality;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.math.BigDecimal;
import java.math.MathContext;

public class BMI {

    /**
     * Tager 2 værdier fra brugeren, beregner BMI'en og viser det til brugeren med en vejledende tekst.
     * For at udregne BMI skal man bruge vægten divideret med brugerens højde i m2. For at få brugerens
     * højde tager vi TextField inputHeight, parser det til en double og dividerer med 100 så vi får
     * vægten i et decimal. Derefter tager vi TextField inputWeight og parser til en double. Så tager
     * vi de to nye variabler og beregner brugerens BMI.
     * <p>
     * I den anden del af vores calculateBMI sp sætter vi vores label showBMI til at vise den beregnet
     * BMI til brugeren. Så har vi en if-statement hvor vi tager brugerens BMI og finder en kategori
     * tilsvarende deres BMI og viser det til brugeren via label showBMICategory.
     *
     * @param inputWeight     Brugerens vægt input i KG
     * @param inputHeight     Brugerens højde input i cm
     * @param showBMI         Et label i vores JavaFX applikation som viser brugerens beregnet BMI
     * @param showBMICategory Et label i vores JavaFX applikation som viser brugerens vægtkategori
     * @see WorkoutTracker
     */
    public void calculateBMI(TextField inputWeight, TextField inputHeight, Label showBMI, Label showBMICategory, ImageView imgDefault, ImageView imgSlim, ImageView imgAverage, ImageView imgFat) {

        try {

            String weightString = inputWeight.getText();
            String heightString = inputHeight.getText();
            double weight = 0;
            double height = 0;

            if (weightString.contains(",")) {
                weightString = weightString.replace(",", ".");
                weight = Double.parseDouble(weightString);
            } else {
                weight = Double.parseDouble(weightString);
            }

            if (heightString.contains(".")) {
                heightString.replace(".", "");
                height = Double.parseDouble(heightString);
            } else {
                height = (Double.parseDouble(inputHeight.getText()) / 100);
            }

            double BMI = weight / (Math.pow(height, 2));

            BigDecimal bd = new BigDecimal(BMI);
            bd = bd.round(new MathContext(3));
            double rounded = bd.doubleValue();

            showBMI.setText("BMI: " + rounded);
            if (BMI < 15) {
                showBMICategory.setText("Meget undervægtig");
                imgDefault.setVisible(false);
                imgAverage.setVisible(false);
                imgFat.setVisible(false);
                imgSlim.setVisible(true);
            } else if (BMI < 18.5) {
                showBMICategory.setText("Undervægtig");
                imgDefault.setVisible(false);
                imgAverage.setVisible(false);
                imgFat.setVisible(false);
                imgSlim.setVisible(true);
            } else if (BMI <= 25) {
                showBMICategory.setText("Normal vægt");
                imgDefault.setVisible(false);
                imgFat.setVisible(false);
                imgSlim.setVisible(false);
                imgAverage.setVisible(true);
            } else if (BMI <= 30) {
                showBMICategory.setText("Overvægtig");
                imgDefault.setVisible(false);
                imgAverage.setVisible(false);
                imgSlim.setVisible(false);
                imgFat.setVisible(true);
            } else if (BMI <= 40) {
                showBMICategory.setText("Fed");
                imgDefault.setVisible(false);
                imgAverage.setVisible(false);
                imgSlim.setVisible(false);
                imgFat.setVisible(true);
            } else if (BMI > 40) {
                showBMICategory.setText("Svært fed");
                imgDefault.setVisible(false);
                imgAverage.setVisible(false);
                imgSlim.setVisible(false);
                imgFat.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("FEJL");
            alert.setHeaderText("Input er ikke tilladt");
            alert.setContentText("Kun tal er tilladt.");
            alert.showAndWait();
        }
    }



}
