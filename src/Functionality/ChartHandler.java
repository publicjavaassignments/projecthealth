package Functionality;

import Database.DB;
import javafx.collections.FXCollections;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;

public class ChartHandler
{

    public void setChart(Label workoutStatusLabel, Label weekAverageID, ImageView workoutStatusIconBd, ImageView workoutStatusIcon, BarChart workoutStatistics) {
        DB.selectSQL("SELECT fldWeekNumber FROM tblExercise where fldRecordID = (SELECT MAX(fldRecordID) FROM tblExercise)");
        String weekNumber1 = Database.DB.getDisplayData();

        DB.selectSQL("SELECT fldWeekNumber FROM tblExercise where fldRecordID = (SELECT MAX(fldRecordID - 1) FROM tblExercise)");
        String weekNumber2 = Database.DB.getDisplayData();

        DB.selectSQL("SELECT fldWeekNumber FROM tblExercise where fldRecordID = (SELECT MAX(fldRecordID - 2) FROM tblExercise)");
        String weekNumber3 = Database.DB.getDisplayData();

        DB.selectSQL("SELECT fldExerciseHours FROM tblExercise where fldRecordID = (SELECT MAX(fldRecordID) FROM tblExercise)");
        String workoutHours1 = Database.DB.getDisplayData();
        double workoutHours1Cast = Double.parseDouble(workoutHours1);

        DB.selectSQL("SELECT fldExerciseHours FROM tblExercise where fldRecordID = (SELECT MAX(fldRecordID - 1) FROM tblExercise)");
        String workoutHours2 = Database.DB.getDisplayData();
        double workoutHours2Cast = Double.parseDouble(workoutHours2);

        DB.selectSQL("SELECT fldExerciseHours FROM tblExercise where fldRecordID = (SELECT MAX(fldRecordID - 2) FROM tblExercise)");
        String workoutHours3 = Database.DB.getDisplayData();
        double workoutHours3Cast = Double.parseDouble(workoutHours3);

        DB.getDisplayData();

        //Defining the axes
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setCategories(FXCollections .<String>
                observableArrayList(Arrays.asList(weekNumber1, weekNumber2, weekNumber3)));
        xAxis.setLabel("Uge");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Timer");

        //Prepare XYChart.Series objects by setting data
        XYChart.Series series1 = new XYChart.Series();
        series1.setName(weekNumber1);
        series1.getData().add(new XYChart.Data(weekNumber1, workoutHours1Cast));

        XYChart.Series series2 = new XYChart.Series();
        series2.setName(weekNumber2);
        series2.getData().add(new XYChart.Data(weekNumber2, workoutHours2Cast));

        XYChart.Series series3 = new XYChart.Series();
        series3.setName(weekNumber3);
        series3.getData().add(new XYChart.Data(weekNumber3, workoutHours3Cast));

        //Setting the data to bar chart
        workoutStatistics.getData().addAll(series1, series2, series3);

        double weekAverage = ((workoutHours3Cast + workoutHours2Cast + workoutHours1Cast)/3);

        BigDecimal bd = new BigDecimal(weekAverage);
        bd = bd.round(new MathContext(2));
        double rounded = bd.doubleValue();

        String weekAverageString = (rounded + "hrs");
        weekAverageID.setText(weekAverageString);

        if (weekAverage > 4)
        {
            workoutStatusLabel.setText("You're doing great!");
            workoutStatusIcon.setVisible(true);
            workoutStatusIconBd.setVisible(false);
        }
        else
        {
            workoutStatusLabel.setText("You're slacking!");
            workoutStatusIcon.setVisible(false);
            workoutStatusIconBd.setVisible(true);
        }
    }

}
