package Functionality;

import Database.DB;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
/**
 * Workout Tracker tager 2 værdier fra brugeren, validerer det og derefter indsætter det i vores Database.
 * Den starter med at parse en int fra TextField weekNumber og en String fra TextField totalHours. Vi benytter en
 * String til totalHours fordi at Databasen ikke kan tage komma'er så ved at tage det som en String kan vi ændre
 * brugerens input fra et komma til et punktum.
 *<p>
 * Når inputtet er valideret indsætter vi værdierne week og hours ind i vores Database via en SQL query.
 * Hele metodens indhold er lavet med en try-catch så vi kan opfange exceptions og display til brugeren at deres
 * input var ikke korrekt. Det forhindrer også at vores Database bliver fyldt med fejlagtige informationer.
 * @see BMI
 */
public class WorkoutTracker {
    public void workoutTracking(TextField weekNumber, TextField totalHours) {

        try {
            int week = (Integer.parseInt(weekNumber.getText()));
            String hoursString = totalHours.getText();
            double hours = 0;

            if (hoursString.contains(",")) {
                hoursString = hoursString.replace(",", ".");
                hours = Double.parseDouble(hoursString);
            } else {
                hours = Double.parseDouble(hoursString);
            }

            DB.insertSQL("insert into tblExercise (fldWeekNumber, fldExerciseHours) values (" + week + ", " + hours + ");");


        } catch (Exception e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("FEJL");
            alert.setHeaderText("Input er ikke tilladt");
            alert.setContentText("Kun tal er tilladt.");
            alert.showAndWait();
        }
    }
}