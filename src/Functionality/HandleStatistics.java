package Functionality;

import Database.DB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class HandleStatistics {

    public void addStatistics(TableView<Statistics> tableView, TableColumn<Statistics, Integer> colWeekNumber, TableColumn<Statistics, Double> colExerciseHours) {
        colWeekNumber.setCellValueFactory(new PropertyValueFactory<Statistics, Integer>("weekNumber"));
        colExerciseHours.setCellValueFactory(new PropertyValueFactory<Statistics, Double>("exerciseHours"));

        tableView.setItems(getStatistics());
    }

    public ObservableList<Statistics> getStatistics() {
        ObservableList<Statistics> statistics = FXCollections.observableArrayList();
        DB.selectSQL("SELECT COUNT(fldRecordID) from tblExercise");
        int counter = Integer.parseInt(DB.getData());

        int weekNumber;
        int recordID;
        double exerciseHours;
        DB.selectSQL("SELECT fldRecordID from tblExercise");
        recordID = Integer.parseInt(DB.getData());

        for(int i = 0; i < counter; i++) {
            DB.selectSQL("SELECT fldWeekNumber from tblExercise where fldRecordID = " + recordID);
            String weekNumberString = DB.getData();
            System.out.println(weekNumberString);
            weekNumber = Integer.parseInt(weekNumberString);

            DB.selectSQL("SELECT fldExerciseHours from tblExercise where fldRecordID = " + recordID);
            exerciseHours = Double.parseDouble(DB.getData());
            System.out.println(exerciseHours);
            statistics.add(new Statistics(weekNumber, exerciseHours));

            recordID++;
        }

        return statistics;
    }

}
