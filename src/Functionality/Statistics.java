package Functionality;

public class Statistics {

    private int weekNumber;
    private double exerciseHours;

    public Statistics(int weekNumber, double exerciseHours) {
        this.weekNumber = weekNumber;
        this.exerciseHours = exerciseHours;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setExerciseHours(double exerciseHours) {
        this.exerciseHours = exerciseHours;
    }

    public double getExerciseHours() {
        return exerciseHours;
    }
}
